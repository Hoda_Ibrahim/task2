<?php

namespace App\Http\Controllers\Auth;

use App\Mail\verifyEmailFirst;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;
use App\Mail\welcomeEmail;
class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }



    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users|regex:/^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(femto15)\.com$/u',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     *
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'verify_token' => Str::random(40),
            'password' => bcrypt($data['password']),
        ]);
        ini_set('max_execution_time', 600);

        $thisUser = User::findOrFail($user->id);
        $this->sendEmail($thisUser);
    }

    public function sendEmail($thisUser)
    {
        ini_set('max_execution_time', 600);
        Mail::to($thisUser['email'])->send(new verifyEmailFirst($thisUser));
    }

    public function verifyEmailFirst()
    {
        return view('email.verifyEmailFirst');
    }

    public function sendEmailDone($email , $verify_token)
    {
        $user = User::where(['email'=> $email , 'verify_token' => $verify_token])->first();
        if ($user) {
            $user->update([
                'status' => '1',
                'verify_token' => NULL
            ]);
            ini_set('max_execution_time', 600);
            Mail::to($email)->send(new welcomeEmail($user));
            $this->guard()->login($user);
            return view('home');
        }

    }
}
